/**
 * Created by pjrowe on 4/14/17.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { ProgramsComponent } from './pages/programs/programs.component';
import { ExercisesComponent } from './pages/exercises/exercises.component';
import { WorkoutsComponent } from './pages/workouts/workouts.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'programs', component: ProgramsComponent},
  { path: 'exercises', component: ExercisesComponent},
  { path: 'workouts', component: WorkoutsComponent},
  { path: '**', redirectTo: ''}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
