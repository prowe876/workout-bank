package org.duttydev.workoutbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkoutBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkoutBankApplication.class, args);
	}
}
