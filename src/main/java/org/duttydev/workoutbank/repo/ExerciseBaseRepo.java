package org.duttydev.workoutbank.repo;

import org.duttydev.workoutbank.domain.exercise.AbstractExercise;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

/**
 * Created by pjrowe on 11/24/17.
 */
@NoRepositoryBean
public interface ExerciseBaseRepo<T extends AbstractExercise> extends Repository<T, Long> {
    T findOne(Long id);
    Iterable<T> findAll();
    Iterable<T> findAll(Sort sort);
    Page<T> findAll(Pageable pageable);
}
