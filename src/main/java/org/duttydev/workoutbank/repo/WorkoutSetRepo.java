package org.duttydev.workoutbank.repo;

import org.duttydev.workoutbank.domain.WorkoutSet;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by pjrowe on 11/24/17.
 */
@Repository
public interface WorkoutSetRepo extends PagingAndSortingRepository<WorkoutSet,Long> {
}
