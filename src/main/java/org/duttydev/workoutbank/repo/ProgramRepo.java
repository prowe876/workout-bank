package org.duttydev.workoutbank.repo;

import org.duttydev.workoutbank.domain.Program;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * Created by pjrowe on 11/23/17.
 */
@Repository
public interface ProgramRepo extends PagingAndSortingRepository<Program,Long> {

}
