package org.duttydev.workoutbank.repo;

import org.duttydev.workoutbank.domain.exercise.AbstractExercise;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by pjrowe on 11/24/17.
 */
@Repository
@Transactional
public interface ExerciseRepo extends ExerciseBaseRepo<AbstractExercise> {

}
