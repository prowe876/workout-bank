package org.duttydev.workoutbank.repo;

import org.duttydev.workoutbank.domain.exercise.AbstractExercise;
import org.duttydev.workoutbank.domain.exercise.ReppedExercise;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by pjrowe on 11/24/17.
 */
@Repository
@Transactional
public interface ReppedExerciseRepo  extends ExerciseBaseRepo<ReppedExercise>, PagingAndSortingRepository<ReppedExercise,Long> {
}
