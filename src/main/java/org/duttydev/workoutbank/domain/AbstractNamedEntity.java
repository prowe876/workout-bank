package org.duttydev.workoutbank.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by pjrowe on 11/24/17.
 */
@MappedSuperclass
public abstract class AbstractNamedEntity extends AbstractEntity {

    @NotNull
    @Basic
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
