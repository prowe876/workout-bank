package org.duttydev.workoutbank.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by pjrowe on 11/23/17.
 */
@Entity
public class Program extends AbstractNamedEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    private Difficulty difficulty;

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Program)) return false;
        if (!super.equals(o)) return false;

        Program program = (Program) o;

        return difficulty == program.difficulty;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (difficulty != null ? difficulty.hashCode() : 0);
        return result;
    }
}
