package org.duttydev.workoutbank.domain;

/**
 * Created by pjrowe on 11/23/17.
 */
public enum Difficulty {
    EASY, MEDIUM, HARD
}
