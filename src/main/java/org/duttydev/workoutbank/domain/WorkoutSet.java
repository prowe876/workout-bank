package org.duttydev.workoutbank.domain;

import org.duttydev.workoutbank.domain.exercise.AbstractExercise;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by pjrowe on 11/24/17.
 */
@Entity
public class WorkoutSet extends AbstractEntity {

    @Basic
    private int numberOfRounds;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "workset_exercise",
            joinColumns = { @JoinColumn(name = "workoutset_id") },
            inverseJoinColumns = { @JoinColumn(name = "exercise_id") }
    )
    private Set<AbstractExercise> exercises = new HashSet<>(0);

    @ManyToOne
    @JoinColumn(name="workout_id", nullable=false)
    private Workout workout;

    public Workout getWorkout() {
        return workout;
    }

    public void setWorkout(Workout workout) {
        this.workout = workout;
    }

    public int getNumberOfRounds() {
        return numberOfRounds;
    }

    public void setNumberOfRounds(int numberOfRounds) {
        this.numberOfRounds = numberOfRounds;
    }

    public Set<AbstractExercise> getExercises() {
        return exercises;
    }

    public void setExercises(Set<AbstractExercise> exercises) {
        this.exercises = exercises;
    }
}
