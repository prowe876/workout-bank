package org.duttydev.workoutbank.domain;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pjrowe on 11/24/17.
 */
@Entity
public class Workout extends AbstractNamedEntity {

    @OneToMany(mappedBy="workout")
    private List<WorkoutSet> workoutSets = new ArrayList<>();

    public List<WorkoutSet> getWorkoutSets() {
        return workoutSets;
    }

    public void setWorkoutSets(List<WorkoutSet> workoutSets) {
        this.workoutSets = workoutSets;
    }
}
