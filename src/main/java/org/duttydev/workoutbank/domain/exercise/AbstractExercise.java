package org.duttydev.workoutbank.domain.exercise;

import org.duttydev.workoutbank.domain.AbstractNamedEntity;
import org.duttydev.workoutbank.domain.WorkoutSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by pjrowe on 11/24/17.
 */
@Entity
@Inheritance
public abstract class AbstractExercise extends AbstractNamedEntity {

    @Column
    private String description;

    @ManyToMany(mappedBy = "exercises")
    private Set<WorkoutSet> sets = new HashSet<>();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
