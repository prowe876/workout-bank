package org.duttydev.workoutbank.domain.exercise;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * Created by pjrowe on 11/24/17.
 */
@Entity
public class ReppedExercise extends AbstractExercise {

    @Basic
    private int numberOfReps;

    public int getNumberOfReps() {
        return numberOfReps;
    }

    public void setNumberOfReps(int numberOfReps) {
        this.numberOfReps = numberOfReps;
    }
}
