package org.duttydev.workoutbank.domain.exercise;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * Created by pjrowe on 11/24/17.
 */
@Entity
public class TimedExercise extends AbstractExercise {

    @Basic
    private int lengthInSeconds;

    public int getLengthInSeconds() {
        return lengthInSeconds;
    }

    public void setLengthInSeconds(int lengthInSeconds) {
        this.lengthInSeconds = lengthInSeconds;
    }
}
